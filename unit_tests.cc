#define CATCH_CONFIG_MAIN
#include "catch.h"

#include "tammes.h"
#include <random>

TEST_CASE("test correctness of random packings", "[correctness]") {
  std::mt19937 gen(1337);
  std::uniform_real_distribution<double> radius_distribution(0.025, 0.1);

  for (int _ = 0; _ < 100; _++) {
    double radius = radius_distribution(gen);
    tammes::packing p = tammes::pack(radius);

    std::vector<tammes::gcs> positions;
    positions.reserve(p.n);
    for (tammes::row ro : p.rows) {
      std::vector<tammes::gcs> new_positions = ro.to_gcs();
      positions.insert(positions.end(), new_positions.begin(),
                       new_positions.end());
    }

    REQUIRE(positions.size() == p.n);

    for (size_t i = 0; i < p.n; i++) {
      for (size_t j = i + 1; j < p.n; j++) {
        double distance =
            tammes::gcs::spherical_distance(positions[i], positions[j]);
        REQUIRE(distance >= Approx(radius * 2.0));
      }
    }
  }
}

TEST_CASE("test nearest neighbors for random packings", "[nearest neighbors]") {
  std::mt19937 gen(1337);
  std::uniform_real_distribution<double> radius_distribution(0.025, 0.1);
  std::uniform_real_distribution<double> lat_distribution(-tammes::PI_2,
                                                          tammes::PI_2);
  std::uniform_real_distribution<double> lon_distribution(0.0, tammes::TAU);

  for (int _ = 0; _ < 100; _++) {
    double radius = radius_distribution(gen);
    const double r2 = radius * radius;
    tammes::packing p = tammes::pack(radius);

    for (int __ = 0; __ < 100; __++) {
      double lat = lat_distribution(gen);
      double lon = lon_distribution(gen);

      tammes::neighbor neighbor = tammes::find_nearest_neighbor_faster(
          tammes::gcs{lat, lon}, p.rows, r2);
      tammes::neighbor neighbor_fast =
          tammes::find_nearest_neighbor_fast(tammes::gcs{lat, lon}, p.rows);

      REQUIRE(neighbor == neighbor_fast);
    }
  }
}

TEST_CASE("test correctness of extended_packing construction",
          "[extended packing]") {
  std::mt19937 gen(1337);
  std::uniform_real_distribution<double> radius_distribution(0.025, 0.1);
  for (int _ = 0; _ < 100; _++) {
    double radius = radius_distribution(gen);
    const double r2 = radius * radius;
    tammes::packing p = tammes::pack(radius);
    tammes::extended_packing ep{p};
    for (size_t i = 0; i < p.rows.size(); ++i) {
      const auto &r = p.rows[i];
      const auto &er = ep.rows[i];
      for (size_t j = 0; j < r.n; ++j) {
        const tammes::eci v = r.to_gcs(j).to_eci();
        const tammes::eci ev = er.to_eci(j);
        REQUIRE(v.approx_equal((ev)));
      }
    }
  }
}
