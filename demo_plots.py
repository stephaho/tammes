#!/usr/bin/env python3

import os

import matplotlib.pyplot as plt
import numpy as np

import sphere_plotting
import tammes

def sphere_circles(f, r):
    n, rows = f(r)
    res = []
    for k, lat, delta_lon in rows:
        lons = delta_lon + np.linspace(0, 2*np.pi, k, endpoint=False)
        for lon in lons:
            res.append(sphere_plotting.SphereCircle((lat, lon), r, label='a'))
    return res

if __name__=='__main__':
    r = 0.06
    camera = (2,1,1.5)
    os.chdir(os.path.expanduser('~/diss/main/figures'))
    with open('demo_naive.tex', 'w') as f:
        sp = sphere_plotting.SpherePlotter(camera = camera, outfile=f)
        sp.default_label_rescale_factor = 0.1
        sp.circles = sphere_circles(tammes.pack_naive,r)
        # plt.figure(1)
        sp.show()
        # plt.waitforbuttonpress()
    with open('demo_improved.tex', 'w') as f:
        sp = sphere_plotting.SpherePlotter(camera = camera, outfile=f)
        sp.default_label_rescale_factor = 0.1
        sp.circles = sphere_circles(tammes.pack_improved,r)
        # plt.figure(2)
        sp.show()
        # plt.waitforbuttonpress()
        # print(sp.calc_local_linear_transform(0,0))
