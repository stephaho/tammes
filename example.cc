#include "tammes.h"

#include <cstdlib>
#include <iomanip>
#include <iostream>

#define DEBUG

int main(int argc, char *argv[]) {
  if (argc == 2 || argc == 3) {
    double radius = atof(argv[1]);

    tammes::packing p;
    if (argc == 3)
      p = tammes::pack(radius, atol(argv[2]));
    else
      p = tammes::pack(radius);

    std::cout << "radius: " << std::setprecision(20) << radius << std::endl;
    std::cout << "number of circles: " << p.n << std::endl;
    std::cout << "coverage: " << std::setprecision(4)
              << tammes::calc_coverage(p.n, radius) << std::endl;

    std::cout << "resulting distance: " << p.get_min_dist() << std::endl;
    return 0;
  } else {
    std::cout << "example <radius> [k]" << std::endl;
    return 1;
  }
}
