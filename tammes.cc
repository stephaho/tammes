#include "tammes.h"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <stdexcept>
#include <unordered_map>
#include <unordered_set>
#include <utility>

namespace tammes {
double calc_coverage(const uint32_t n, const double r) {
  return ((double)n * TAU * (1.0 - cos(r))) / (4.0 * PI);
}

template <typename T> T fast_mod(const T a, const T b) {
  // return a % b, given that a < 2 * b
  assert(a < (b << 1));
  return a - b * (a >= b);
}

double r_circle_to_sphere(const double r) { return sin(r * 0.5) * 2.0; }

bool intersect_circles_on_sphere(const double r, const double lat,
                                 const double lon_between) {
  eci sphere_center = gcs{lat, lon_between * 0.5}.to_eci();
  double sphere_radius = r_circle_to_sphere(r);
  double d = vec3::dot(vec3{0.0, 1.0, 0.0}, sphere_center.co);
  return d < sphere_radius;
}

// https://stackoverflow.com/a/18654302
gcs find_next_row_start(const gcs &a, const gcs &b, const double r,
                        bool &out_found) {
  out_found = false;

  vec3 center_a = a.to_eci().co;
  vec3 center_b = b.to_eci().co;
  vec3 center_c = vec3{0.0, 0.0, 0.0};
  double r_ab = r_circle_to_sphere(r) * 2.0;
  double r_c = 1.0;

  vec3 dir_x = center_b - center_a;
  vec3 e_x = dir_x.normalized();

  vec3 dir_y = center_c - center_a;
  double i = vec3::dot(e_x, dir_y);
  vec3 e_y = (dir_y - i * e_x).normalized();

  vec3 e_z = vec3::cross(e_x, e_y);
  double d = dir_x.length();
  double j = vec3::dot(e_y, dir_y);

  double x = d * 0.5;
  double y = (r_ab * r_ab - r_c * r_c - 2 * i * x + i * i + j * j) / (2 * j);

  double z_sq = r_ab * r_ab - x * x - y * y;
  if (z_sq < 0.0)
    return gcs{};
  double z = sqrt(z_sq);

  vec3 p1 = center_a + x * e_x + y * e_y + z * e_z;
  vec3 p2 = center_a + x * e_x + y * e_y - z * e_z;
  vec3 p = p1.z > p2.z ? p1 : p2;

  double new_lat = asin(p.z);
  if (new_lat < a.lat or new_lat < b.lat)
    return gcs{};

  out_found = true;
  return eci{p}.to_gcs();
}

packing pack_hemisphere(const double r, const uint32_t k, int32_t exponent,
                        std::vector<row> rows = {}) {
  uint32_t n = 0;
  double d = r * 2.0;
  gcs row_start = {0.0, 0.0};

  while (true) {
    uint32_t n_new = k * (uint32_t)pow(2, exponent);
    n += n_new;
    rows.push_back(row{n_new, row_start});

    bool found_a, found_b = false;
    gcs next_row_start =
        find_next_row_start(row_start, rows.end()[-1].to_gcs(1), r, found_a);
    if (rows.size() >= 2) {
      gcs next_row_start_b =
          find_next_row_start(row_start, rows.end()[-2].to_gcs(1), r, found_b);
      if (found_b and (!found_a or next_row_start_b.lat > next_row_start.lat))
        next_row_start = next_row_start_b;
    }

    if (!(found_a or found_b))
      break;

    while (exponent > -2 and
           intersect_circles_on_sphere(r, next_row_start.lat,
                                       TAU / (k * (uint32_t)pow(2, exponent))))
      exponent--;

    if (exponent < 0 or PI_2 - next_row_start.lat < r)
      break;

    row_start = next_row_start;
  }

  double lat = row_start.lat;
  uint32_t max_iter = (uint32_t)((PI_2 - lat - r) / d);
  for (uint32_t _ = 0; _ < max_iter; _++) {
    lat += d;
    uint32_t n_new = (uint32_t)(TAU * cos(lat) / d) - 1;

    while (n_new > 0 and
           intersect_circles_on_sphere(r, lat, TAU / (double)n_new))
      n_new--;

    if (n_new == 0)
      break;

    rows.push_back(row{n_new, gcs{lat, 0.0}});
    n += n_new;
  }

  if (lat < (PI_2 - d)) {
    rows.push_back(row{1, gcs{PI_2, 0.0}});
    n++;
  }

  return packing{n, rows};
}

packing pack(const double r, const std::vector<uint32_t> &ks) {
  packing best_packing = packing{0, std::vector<row>()};

  for (uint32_t k : ks) {
    packing new_packing = pack(r, k);
    if (new_packing.n > best_packing.n)
      best_packing = new_packing;
  }

  return best_packing;
}

packing pack(const double r, const uint32_t k) {
  int32_t exponent = (int32_t)log2(TAU / (r * 2.0) / k);
  packing upper = pack_hemisphere(r, k, exponent);
  packing lower =
      pack_hemisphere(r, k, exponent,
                      {row{upper.rows[1].n, gcs{-upper.rows[1].start.lat,
                                                -upper.rows[1].start.lon}}});

  std::vector<row> all_rows(lower.rows.size() - 2);
  std::transform(lower.rows.rbegin(), lower.rows.rend() - 2, all_rows.begin(),
                 [](const row &ro) {
                   return row{ro.n, gcs{-ro.start.lat, ro.start.lon}};
                 });
  all_rows.insert(all_rows.end(), upper.rows.begin(), upper.rows.end());

  return packing{upper.n + lower.n - lower.rows[1].n, all_rows};
}

/**
 * \brief return the index of the value in `points` that is closest to `target`
 *
 * FIXME: it is inefficient to use this function if all `points` have the same
 *        `lat`.
 */
size_t find_nearest_neighbor(const gcs &target,
                             const std::vector<gcs> &points) {
  eci pos = target.to_eci();

  size_t nearest = 0;
  double min_distance = vec3::distance_sq(pos.co, points[0].to_eci().co);
  for (size_t i = 1; i < points.size(); i++) {
    double distance = vec3::distance_sq(pos.co, points[i].to_eci().co);
    if (distance < min_distance) {
      nearest = i;
      min_distance = distance;
    }
  }

  return nearest;
}

neighbor find_nearest_neighbor_safe(const gcs &target,
                                    const std::vector<row> &rows) {
  std::vector<size_t> column_indices(rows.size());
  std::vector<gcs> nearest_per_row(rows.size());
  for (size_t i = 0; i < rows.size(); i++) {
    row ro = rows[i];
    std::vector<gcs> positions = ro.to_gcs();
    size_t nearest = find_nearest_neighbor(target, positions);
    column_indices[i] = nearest;
    nearest_per_row[i] = positions[nearest];
  }

  size_t row_index = find_nearest_neighbor(target, nearest_per_row);

  return neighbor{row_index, column_indices[row_index]};
}

/**
 * FIXME: explicit construction of various candidate containers is expensive and
 * inefficient
 *
 * Strategy for more efficient impl:
 * - determine min row index, max row index of candidate rows; candidate rows
 *   are the for rows with closest latitude.
 * - simple for loop over candidate rows index range
 *   - compute square-distance-based nearest neighbor
 *
 */
neighbor find_nearest_neighbor_fast(const gcs &target,
                                    const std::vector<row> &rows) {
  size_t n_rows = rows.size();

  if (fabs(target.lat + PI_2) < 0.001)
    return neighbor{0, find_nearest_neighbor(target, rows[0].to_gcs())};
  if (fabs(target.lat - PI_2) < 0.001)
    return neighbor{n_rows - 1,
                    find_nearest_neighbor(target, rows[n_rows - 1].to_gcs())};
  size_t row_index_a = std::clamp<size_t>(
      round(n_rows * (target.lat / PI + 0.5)), 1, n_rows - 2);

  int32_t direction =
      (int32_t)(rows[row_index_a].start.lat < target.lat) * 2 - 1;
  while (row_index_a > 0 and row_index_a < n_rows - 1 and
         rows[row_index_a].start.lat * direction < target.lat * direction)
    row_index_a += direction;

  // FIXME: replace the exensive unordered_set-based construction with an
  // index-based construction
  size_t row_index_b =
      std::clamp<size_t>(row_index_a + direction, 0, n_rows - 1);
  size_t row_index_c =
      std::clamp<size_t>(row_index_a - direction, 0, n_rows - 1);
  size_t row_index_d =
      std::clamp<size_t>(row_index_a - direction * 2, 0, n_rows - 1);

  std::unordered_set<size_t> row_indices_set{row_index_a, row_index_b,
                                             row_index_c, row_index_d};
  std::vector<size_t> row_indices(row_indices_set.begin(),
                                  row_indices_set.end());

  std::vector<size_t> column_indices(row_indices.size());
  std::transform(row_indices.begin(), row_indices.end(), column_indices.begin(),
                 [target, rows](const size_t row_index) {
                   row ro = rows[row_index];
                   return ((size_t)round((double)ro.n *
                                         (target.lon - ro.start.lon + TAU) /
                                         TAU)) %
                          ro.n;
                 });

  std::vector<gcs> nearest_per_row(rows.size());
  for (size_t i = 0; i < row_indices.size(); i++)
    nearest_per_row[i] = rows[row_indices[i]].to_gcs(column_indices[i]);

  size_t nearest = find_nearest_neighbor(target, nearest_per_row);
  return neighbor{row_indices[nearest], column_indices[nearest]};
}

neighbor find_nearest_neighbor_faster(const gcs &target,
                                      const std::vector<row> &rows,
                                      const double r2) {
  const size_t n_rows = rows.size();

  if (fabs(target.lat + PI_2) < 0.001)
    return neighbor{0, find_nearest_neighbor(target, rows[0].to_gcs())};
  if (fabs(target.lat - PI_2) < 0.001)
    return neighbor{n_rows - 1,
                    find_nearest_neighbor(target, rows[n_rows - 1].to_gcs())};
  size_t row_index_a = std::clamp<size_t>(
      round(n_rows * (target.lat / PI + 0.5)), 1, n_rows - 2);
  size_t row_index_b;
  bool short_circuit = false;
  if (rows[row_index_a].start.lat < target.lat) {
    row_index_b = row_index_a + 1;
    while (rows[row_index_b].start.lat < target.lat) {
      if (row_index_b == n_rows - 1) {
        short_circuit = true;
        row_index_a = row_index_b - 1;
        break;
      }
      row_index_b++;
    }
    row_index_a = row_index_a - 1;
  } else {
    while (!(rows[row_index_a].start.lat < target.lat)) {
      if (row_index_a == 0) {
        short_circuit = true;
        row_index_b = 1;
        break;
      }
      row_index_a--;
    }
    row_index_b = row_index_a + 1;
  }
  if (!short_circuit) {
    assert(rows[row_index_a].start.lat < target.lat);
    assert(target.lat <= rows[row_index_b].start.lat);
    if (row_index_a)
      row_index_a--;
    if (row_index_b < n_rows - 1)
      row_index_b++;
  }
  row_index_b++;
  assert(row_index_b <= n_rows);
  assert(row_index_b > row_index_a);
  assert(row_index_a < n_rows);
  // find mindist along the rows index range [row_index_a, row_index_b)
  double min_dist_sqr = 8.0;
  size_t row_index_best = 0;
  size_t column_index_best = 0;
  const eci target_pos = target.to_eci();
  for (size_t ri = row_index_a; ri < row_index_b; ++ri) {
    const row &ro = rows[ri];
    const size_t ci = ((size_t)round((double)ro.n *
                                     (target.lon - ro.start.lon + TAU) / TAU)) %
                      ro.n;
    const eci candidate_pos = ro.to_gcs(ci).to_eci();
    const double dist_sqr = vec3::distance_sq(target_pos.co, candidate_pos.co);
    if (dist_sqr < r2)
      return neighbor{ri, ci};
    if (dist_sqr < min_dist_sqr) {
      min_dist_sqr = dist_sqr;
      row_index_best = ri;
      column_index_best = ci;
    }
  }
  return neighbor{row_index_best, column_index_best};
}

inline neighbor
find_nearest_neighbor_faster(const gcs &target_gcs, const eci &target_eci,
                             const std::vector<extended_row> &rows,
                             const double r2) {
  const size_t n_rows = rows.size();
  size_t row_index_a = std::clamp<size_t>(
      round(n_rows * (target_gcs.lat / PI + 0.5)), 1, n_rows - 2);
  size_t row_index_b;
  bool short_circuit = false;
  if (rows[row_index_a].sin_theta < target_eci.co.z) {
    row_index_b = row_index_a + 1;
    while (rows[row_index_b].sin_theta < target_eci.co.z) {
      if (row_index_b == n_rows - 1) {
        short_circuit = true;
        row_index_a = row_index_b - 1;
        break;
      }
      row_index_b++;
    }
    row_index_a = row_index_a - 1;
  } else {
    while (!(rows[row_index_a].sin_theta < target_eci.co.z)) {
      if (row_index_a == 0) {
        short_circuit = true;
        row_index_b = 1;
        break;
      }
      row_index_a--;
    }
    row_index_b = row_index_a + 1;
  }
  if (!short_circuit) {
    assert(rows[row_index_a].sin_theta < target_eci.co.z);
    assert(target_eci.co.z <= rows[row_index_b].sin_theta);
    row_index_a -= (row_index_a != 0);
    row_index_b += (row_index_b != n_rows - 1);
  }
  row_index_b++;
  assert(row_index_b <= n_rows);
  assert(row_index_b > row_index_a);
  assert(row_index_a < n_rows);
  // find mindist along the rows index range [row_index_a, row_index_b)
  double min_dist_sqr = 8.0;
  size_t row_index_best = 0;
  size_t column_index_best = 0;
  for (size_t ri = row_index_a; ri < row_index_b; ++ri) {
    const extended_row &ro = rows[ri];
    const size_t ci =
        // fast_mod<size_t>(round((double)ro.n * (target_gcs.lon - ro.phi_offset
        // + TAU) /
        //                  TAU), ro.n);
        fast_mod<size_t>(.5 + (double)ro.n *
                                  (target_gcs.lon - ro.phi_offset + TAU) / TAU,
                         ro.n);
    const eci candidate_pos = ro.to_eci(ci);
    // const double phi = ro.phi_offset + ci * ro.delta_phi;
    // const eci candidate_pos { vec3 { cos(phi) * ro.cos_theta, sin(phi) *
    // ro.cos_theta, ro.sin_theta }};
    const double dist_sqr = vec3::distance_sq(target_eci.co, candidate_pos.co);
    if (dist_sqr < r2)
      return neighbor{ri, ci};
    if (dist_sqr < min_dist_sqr) {
      min_dist_sqr = dist_sqr;
      row_index_best = ri;
      column_index_best = ci;
    }
  }
  return neighbor{row_index_best, column_index_best};
}

neighbor find_nearest_neighbor_faster(const eci &target,
                                      const std::vector<extended_row> &rows,
                                      const double r2) {
  return find_nearest_neighbor_faster(target.to_gcs(), target, rows, r2);
}

neighbor find_nearest_neighbor_faster(const gcs &target,
                                      const std::vector<extended_row> &rows,
                                      const double r2) {
  return find_nearest_neighbor_faster(target, target.to_eci(), rows, r2);
}

bool operator==(const neighbor &a, const neighbor &b) {
  return a.row_index == b.row_index and a.column_index == b.column_index;
}

std::ostream &operator<<(std::ostream &stream, const neighbor &neighbor) {
  return stream << "(" << neighbor.row_index << " " << neighbor.column_index
                << ")";
}

gcs packing::neighbor_to_gcs(const neighbor &n) const {
  return rows[n.row_index].to_gcs(n.column_index);
}

double packing::get_min_dist() const {
  size_t n_mid = rows.size() / 2;
  return gcs::spherical_distance(rows[n_mid].to_gcs(0),
                                 rows[n_mid + 1].to_gcs(0));
}

gcs row::to_gcs(const uint32_t index) const {
  return gcs{this->start.lat,
             this->start.lon + TAU * (double)index / (double)this->n};
}

std::vector<gcs> row::to_gcs() const {
  auto result = std::vector<gcs>(this->n);

  for (uint32_t i = 0; i < this->n; i++)
    result[i] = this->to_gcs(i);

  return result;
}

eci gcs::to_eci() const {
  double cos_lat = cos(this->lat);
  return eci{
      vec3{cos_lat * cos(this->lon), cos_lat * sin(this->lon), sin(this->lat)}};
}

// https://en.wikipedia.org/wiki/Great-circle_distance#Computational_formulas
double gcs::spherical_distance(const gcs &a, const gcs &b) {
  return 2 * asin(sqrt(pow(sin(fabs(a.lat - b.lat) * 0.5), 2) +
                       cos(a.lat) * cos(b.lat) *
                           pow(sin(fabs(a.lon - b.lon) * 0.5), 2)));
}

gcs eci::to_gcs() const {
  return gcs{asin(this->co.z), atan2(this->co.y, this->co.x)};
}

bool eci::approx_equal(const eci &other) const {
  constexpr double max_diff = 1e-10;
  return (std::abs(co.x - other.co.x) < max_diff &&
          std::abs(co.y - other.co.y) < max_diff &&
          std::abs(co.z - other.co.z) < max_diff);
}

xy ::xy(const double _x, const double _y) : x(_x), y(_y) {}

rot2d::rot2d(const double phi) : cos_phi(cos(phi)), sin_phi(sin(phi)) {}

xy rot2d::rotate(const xy &v) const {
  return {cos_phi * v.x - sin_phi * v.y, sin_phi * v.x + cos_phi * v.y};
}

std::pair<uint8_t, uint8_t> factor_odd_power2(uint32_t n) {
  // compute the pair (a,b) such that a is odd and a << b == n
  uint8_t b = 0;
  if (n == 0)
    throw std::runtime_error("cannot factor 0");
  while ((n & 1) == 0) {
    b++;
    n >>= 1;
  }
  return {n, b};
}

extended_row::extended_row(uint16_t _n, double _theta, double _phi_offset)
    : n(_n), k(0), j(0), cos_theta(cos(_theta)), sin_theta(sin(_theta)),
      phi_offset(_phi_offset), phi_offset_matrix(_phi_offset),
      xy_cache(nullptr), cache_stride_log2(0) {
  std::tie(k, j) = factor_odd_power2(n);
};

eci extended_row::to_eci(const uint32_t index) const {
  assert(index < (k << j));
  const xy res =
      phi_offset_matrix.rotate((*xy_cache)[index << cache_stride_log2]);
  return eci{vec3{res.x * cos_theta, res.y * cos_theta, sin_theta}};
}

extended_packing::extended_packing(const packing &p) : n(p.n) {
  std::unordered_map<uint8_t, std::pair<uint8_t, uint8_t>> k_map;
  // k_map[k] == (<index in xy_caches>, max {j})
  rows.reserve(p.rows.size());
  // iterate over p.rows to initialize own rows
  for (const auto &row : p.rows) {
    uint8_t j, k;
    std::tie(k, j) = factor_odd_power2(row.n);
    rows.emplace_back(row.n, row.start.lat, row.start.lon);
    auto it = k_map.find(k);
    if (it == k_map.end()) {
      k_map.emplace(k, std::make_pair(xy_caches.size(), j));
      xy_caches.emplace_back();
    } else {
      it->second.second = std::max(it->second.second, j);
    }
  }
  // precompute xy_caches
  for (const auto &item : k_map) {
    const size_t i = item.second.first;
    const uint32_t nn = item.first << item.second.second;
    xy_caches[i].reserve(nn);
    for (size_t j = 0; j < nn; ++j) {
      const double phi = (TAU * (double)j) / nn;
      xy_caches[i].emplace_back(cos(phi), sin(phi));
    }
  }
  // set references to xy_caches
  for (auto &row : rows) {
    uint8_t max_j;
    uint8_t index;
    std::tie(index, max_j) = k_map[row.k];
    row.xy_cache = &(xy_caches[index]);
    assert(index < xy_caches.size());
    assert(max_j >= row.j);
    row.cache_stride_log2 = max_j - row.j;
    assert(row.xy_cache->size() == row.n << row.cache_stride_log2);
  }
}

} // namespace tammes
