"""Spherical geometry stuff.

The primitives are used by sphere_plotting.py (project tammes).

The random waypoint stuff like `random_waypoint_paths_to_file()` is used by
`ns3-transponder/eval/full_limb_eval.py`.

"""

import numpy as np
from numpy.linalg import norm

R_EARTH_MEAN = 6371.0e3 # Earth mean radius in meter
NM = 1852 # nautical mile in meter
HOUR = 3600 # seconds in one hour

KNOTS2RAD_PER_SECOND_FACTOR = np.double(NM/(HOUR*R_EARTH_MEAN))

def angle(p0, p1):
    return angle_normed(normed(p0), normed(p1))

def angular_range_complement(rng):
    """Given an angular range as (frm, to) where frm < to, return (frm2, to2)
    with frm2 < to2 representing the set-wise complement in [0, 2 Pi]."""
    if rng[1] > np.pi:
        return (rng[1] - 2*np.pi, rng[0])
    return (rng[1], rng[0] + 2*np.pi)

def angle_normed(u, v):
    """ Given two unit vectors p0, p1 in R^3, compute the angle between them.

    Returns: angle in [0, pi]
    https://math.stackexchange.com/a/1782769
    """
    return 2 * np.arctan2(norm(u-v), norm(u+v))

def deg2rad(x):
    if not isinstance(x, (np.ndarray, int, float)):
        x = np.array(x, dtype=np.double)
    return np.pi / 180.0 * x

def rad2deg(x):
    return 180.0 / np.pi * x

def knots2rad_per_second(v):
    """Convert a speed given in knots (nautical miles per hour) to radians per second."""
    return v * KNOTS2RAD_PER_SECOND_FACTOR

def meters_per_second2rad_per_second(v):
    """Convert a speed given in m/s to radians per second."""
    return v / R_EARTH_MEAN

def intersect_planes_sample_vector(a, b):
    """Given vectors `a` and `b`, return linear combination c.

    c is the linear combination of `a` and `b` that lies on the intersection
    of Plane(a) and Plane(b).

    Here, Plane(x) is defined as the plane both
    - perpendicular to x
    - and through x.
    """
    a2 = np.dot(a, a) # a * a
    b2 = np.dot(b, b) # b * b
    ab = np.dot(a, b) # a * b
    alpha, beta = np.linalg.solve([[a2, ab], [ab, b2]], [a2, b2])
    return alpha * to_ndarray(a) + beta * to_ndarray(b)

def normed(v):
    """Compute unit vector.
    If v is 2-dim ndarray, return a 2-dim array with normed columns."""
    if len(v.shape) == 1:
        return v / np.linalg.norm(v)
    else:
        return v / np.linalg.norm(v, axis=0).reshape((1,-1))

def normalize_longitude(x):
    """Return y in [-pi,pi) such that x === y (mod 2pi)"""
    if -np.pi <= x < np.pi:
        return x
    return (x + np.pi) % (2 * np.pi) - np.pi

def rotate_to_lat_lon_matrix(theta, phi):
    """ Return rotation matrix R that rotates [1, 0, 0] to theta phi.

    R * [1, 0, 0] == [cos(phi) cos(theta), sin(phi) cos(theta), sin(theta)]
    """
    cp = np.cos(phi)
    sp = np.sin(phi)
    ct = np.cos(theta)
    st = np.sin(theta)
    return np.array([
        [cp * ct, -sp, -cp * st],
        [sp * ct,  cp, -sp * st],
        [     st,   0,       ct],
        ])

def rot_x(phi):
    """Rotation matrix about the x-axis."""
    cp = np.cos(phi)
    sp = np.sin(phi)
    return np.array([[1,  0,   0],
                     [0, cp, -sp],
                     [0, sp,  cp],
                     ])

def rot_y(phi):
    """Rotation matrix about the y-axis."""
    cp = np.cos(phi)
    sp = np.sin(phi)
    return np.array([[cp,  0, sp],
                     [0,   1,  0],
                     [-sp, 0, cp],
                     ])

def rot_z(phi):
    """Rotation matrix about the z-axis."""
    cp = np.cos(phi)
    sp = np.sin(phi)
    return np.array([[cp, -sp, 0],
                     [sp,  cp, 0],
                     [0,    0, 1]])

def rot2d(phi):
    """Rotation matrix in 2 dimensions."""
    cp = np.cos(phi)
    sp = np.sin(phi)
    return np.array([[cp, -sp],
                     [sp,  cp],
                     ])
def to_latlon(p):
    """ Convert cartesian cooridnates to pair (lat, lon) in radians. Idempotent.
    """
    if len(p) == 2:
        return p
    x, y, z = p
    lon = np.arctan2(y, x)
    lat = np.arctan2(z, np.sqrt(x*x + y*y))
    return (lat, lon)

def to_cart_S2(latlon):
    """Given lat and lon, return an appropriate unit vector in R^3.

    - idempotent

    Convention:
        0, 0    -> [1, 0, 0]
        0, pi/2 -> [0, 1, 0]
     pi/2, *    -> [0, 0, 1]
    -pi/2, *    -> [0, 0,-1]

    Returns:
      numpy.ndarray with shape == (3,)
    """
    if len(latlon) == 3:
        return np.array(latlon)
    theta, phi = latlon
    ct = np.cos(theta)
    st = np.sin(theta)
    cp = np.cos(phi)
    sp = np.sin(phi)
    return np.array([cp*ct, sp*ct, st])

def to_ndarray(a):
    """Convert to ndarray if necessary."""
    if isinstance(a, np.ndarray):
        return a
    return np.array(a)

def get_course(pivot, target):
    """Get the course to take from pivot in order to reach target.

    This is the inverse of the second parameter of propagate_course_distance:

    get_cource(a, propagate_course_distance(a, c, d)) == c

    for all c in (-pi, pi), d in (0, pi)
    """
    lat, lon = to_latlon(pivot)
    x, y, z = rotate_to_lat_lon_matrix(lat, lon).T @ to_cart_S2(target)
    return np.arctan2(y, z)

def propagate_course_distance(p, c, d):
    """Given a point p in S_2, a course c in [0, 2pi] and a distance d, compute the point q
    that is reached when starting from p moving d in direction c.

    c = corresponds to 'North'
    for d = N * 2pi, p is returned.
    """
    # construct q wrt p=[1, 0, 0] and c=0
    q = np.array([np.cos(d), 0, np.sin(d)])
    # rotate around x-axis by angle c to get course right
    # note that couse is counted clockwise, i.e., East == pi/2
    q = rot_x(-c) @ q
    lat, lon = to_latlon(p)
    # rotate around y-axis to move [1, 0, 0] to lat
    q = rot_y(-lat) @ q
    # rotate around z-axis to arrive at correct lon
    q = rot_z(lon) @ q
    return q

def arctan2r(y, x):
    """Given that
      x = r * cos(phi)
      y = r * sin(phi)

    return phi, r
    """
    return (np.arctan2(y, x), np.sqrt(x*x + y*y))

def slerp_inverse_lat(p, q, lat, sgn_dz_dt):
    r"""For a slerp defined by the points p and q, compute the smallest non-negative
    value of t such that

      Slerp(p, q)(t)[2] == sin(lat)

    and
          /  d                   \
      sgn | -- Slerp(p, q)(t)[2] | == sgn_dz_dt
          \ dt                   /

    Raise a `DomainError` if the geodesic does not intersect the given latitude circle.

    WARNING: The tangential case might yield undefined behaviour; it is not treated specially.
    """
    Omega = angle(p, q)
    pz = p[2]
    qz = q[2]
    # sin(Omega - Omega t) pz + sin(Omega t) qz = sin(Omega) sin(lat)
    # sin(Omega t) * (qz - pz * cos(Omega)) + cos(Omega t) sin(Omega) pz = sin(Omega) sin(lat)
    beta, d = arctan2r(np.sin(Omega) * pz, qz - pz * np.cos(Omega))
    # d * sin(Omega t) * cos(beta) + d cos(Omega t) sin(beta) = sin(Omega) sin(lat)
    # sin(Omega t + beta) = sin(Omega) sin(lat) / d
    k = np.sin(Omega) * np.sin(lat) / d
    if not -1 <= k <= 1:
        raise DomainError(f'cannot solve `sin(k) == {k}`')
    arcsin_k = np.arcsin(k)
    # expanation:
    # in the non-tangential case, the slerp's geodesic has two intersections with the given latitude circle.
    # arcsin_k in [-pi/2,pi/2] yields one of these intersections, the other comes from, e.g. `pi - arcsin_k`
    # So we simply test both candidates for sign of the derivative.
    #
    # Note that
    # dz/dt = Omega/sin(Omega) (qz cos(Omega t) - pz cos(Omega - Omega t))
    Omega_t = np.array([(arcsin_k - beta) % (2*np.pi), (np.pi - arcsin_k - beta) % (2*np.pi)])
    dzdt = qz * np.cos(Omega_t) - pz * np.cos(Omega - Omega_t)
    assert dzdt[0] * dzdt[1] <= 0
    if np.sign(dzdt[0]) == sgn_dz_dt:
        return Omega_t[0] / Omega
    else:
        return Omega_t[1] / Omega

def slerp_inverse_lon(p, q, lon):
    """For a slerp defined by the points p and q, compute the value of t such that

    arctan2(s[1], s[0]) = lon  where  s == Slerp(p, q)(t)

    Given that Omega is the angle between p and q, the solution set of this function is
    (2*pi/Omega)-periodic. Out of these values, the smallest non-negative is returned.

    Algorithm implemented:
      - rotate p and q around z-axis by -lon
      - solve Slerp(p', q')(t)[1] == 0  for  t
        - this yiels either the correct t or a value t' leading to `lon + pi`
      - select the correct value by testing wether the x-coordinate of the rotated slerp is negative
    """
    Omega = angle(p, q)
    # from now on only x and y coordinates matter;
    # rotate p and q around z axis
    cl = np.cos(lon)
    sl = np.sin(lon)
    px = cl * p[0] + sl * p[1]
    qx = cl * q[0] + sl * q[1]
    py = -sl * p[0] + cl * p[1]
    qy = -sl * q[0] + cl * q[1]
    # using arctan2 avoids fractions:
    tOmega = np.arctan2(py * np.sin(Omega), py * np.cos(Omega) - qy)
    while tOmega < 0:
        tOmega += np.pi
    if np.sin(Omega - tOmega) * px + np.sin(tOmega) * qx < 0:
        return (tOmega + np.pi) / Omega
    return tOmega / Omega

class DomainError(ValueError):
    """Raised by an inverse function if there doesnt exist a solution for the given parameter(s)."""

class Slerp:
    """Spherical linear interpol + d cos(Omega t) sin(beta)ation.

    Let Omega = anlge(p0, O, p1)

    slerp(p0, p1)(t) := (sin((1-t)*Omega) * p0 + sin(t*Omega) * p1) / sin(Omega)

    For small Omega, this becomes instable / undefined and is thus replaced by linear interpolation.
    """
    def __init__(self, p0, p1):
        self.p0_cart = to_cart_S2(p0)
        self.p1_cart = to_cart_S2(p1)
        self.p0 = self.p0_cart.copy()
        self.p1 = self.p1_cart.copy()
        self.Omega = angle(self.p0, self.p1)
        self.linear = self.Omega < 2e-8
        if not self.linear:
            sin_Omega1 = 1/np.sin(self.Omega)
            self.p0 *= sin_Omega1
            self.p1 *= sin_Omega1
    def __call__(self, t, check=False):
        if self.linear:
            return (1-t) * self.p0 + t * self.p1
        tOmega = t * self.Omega
        res = np.sin(self.Omega - tOmega) * self.p0 + np.sin(tOmega) * self.p1
        if check:
            assert np.linalg.det((self.p0_cart, self.p1_cart, res)) < 1e-14
            assert np.abs(angle(res, self.p0_cart) - t * self.Omega) < 1e-14
            assert np.abs(angle(res, self.p1_cart) - (1-t) * self.Omega) < 1e-14
        return res

def lat_lon_rect_area(lat_range, lon_range):
    """
    the unisphere area of the latitude-longitude rectangle given by ranges
    A = int dphi int dtheta cos(theta)
    """
    return (lon_range[1] - lon_range[0]) * (np.sin(lat_range[1]) - np.sin(lat_range[0]))

def get_lat_lon_rect_from_cartesian_rect(cart_height, cart_width, center_lat_lon):
    """Construct lat_range, lon_range, so that
    - the resulting area on a sphere with radius REarth equals cart_height*cart_width
    - the center (mean(lat_range), mean(lon_range)) == center_lat_lon
    - (lat_range[1] - lat_range[0]) * REarth == cart_height

    Units:
    - cart_height and cart_width are given in meters
    - center_lat_lon is given in rad
    - the results are also in rad
    """
    lat_r2 = cart_height / R_EARTH_MEAN / 2
    lat_range = [center_lat_lon[0] - lat_r2, center_lat_lon[0] + lat_r2]
    lon_r2 = cart_height * cart_width / (R_EARTH_MEAN**2 * (np.sin(lat_range[1]) - np.sin(lat_range[0]))) / 2
    lon_range = [center_lat_lon[1] - lon_r2, center_lat_lon[1] + lon_r2]
    return lat_range, lon_range

def random_point_in_latlon_rect(lat_range, lon_range):
    """Uniformly sample points from the S2 subset lat_range X lon_range.

    Uniformity is meant w.r.t. the S2 measure, and _not_ in lat,lon parameter space.

    This means that the longitude pdf is itself uniform, but the latitude pdf monotonically
    decreases with abs(lat). The implementation uses the fact that sin(lat) is a uniform random
    variable for (lat,lon) sampled uniformly from S2.
    """
    z_range = np.sin(lat_range)
    z = z_range[0] + np.random.rand() * np.diff(z_range)[0]
    lon = lon_range[0] + np.random.rand() * np.diff(lon_range)[0]
    return (np.arcsin(z), lon)

def next_or_random(it):
    if it is not None:
        try:
            return next(it)
        except StopIteration:
            pass
    return np.random.random_sample()

def random_waypoint_path(lat_range, lon_range, vmax, tmax, duration,
                          initial_pos=None,
                          courses=None,
                          speeds=None,
                          times=None):
    """Generator of sequence of tuples (time, lat, lon).

    The generated path is a box-reflexion random walk in the S2 subset
    defined by the (lat,lon)-space rectangle lat_range X lon_range.

    Uniform geodesic motion between the waypoints is assumed.

    Properties:
    - The resulting pdf of node position is uniform wrt to S2 volume measure.
    - The velocity direction (aka course) distribution is uniform on [0, 2*pi).
    - The velocity magnitude (aka speed) distribution is uniform on [0, vmax].
    - Reflections at box walls locally resemble the laws of specular reflexion in the
      reflexion point's local tangent space.
    """
    assert np.diff(lat_range)[0] > 0
    assert np.diff(lon_range)[0] > 0
    eps = 1e-6
    if initial_pos is None:
        last_wp = random_point_in_latlon_rect(lat_range, lon_range)
    else:
        last_wp = initial_pos
    last_wp_cart = to_cart_S2(last_wp)
    last_t = 0
    yield (last_t, *last_wp)
    while True:
        # import ipdb; ipdb.set_trace()
        delta_t = next_or_random(times) * tmax
        next_t = last_t + delta_t
        v = next_or_random(speeds) * vmax
        distance = v * delta_t
        assert distance < np.pi
        next_wp_cart = propagate_course_distance(last_wp, next_or_random(courses)*np.pi*2, distance)
        next_wp = to_latlon(next_wp_cart)
        while True:
            t_reflect_candidates = []
            # latitude reflexions:
            for lat, sgn_d in zip(lat_range, (-1, 1)):
                try:
                    t = slerp_inverse_lat(last_wp_cart, next_wp_cart, lat, sgn_d)
                except DomainError:
                    continue
                assert t >= 0
                if t > 1:
                    continue
                t_reflect_candidates.append((t, np.pi))
            # the only possible lon reflexion, depending on np.sign(course)
            if get_course(last_wp, next_wp) > 0:
                lon = lon_range[1]
            else:
                lon = lon_range[0]
            t = slerp_inverse_lon(last_wp_cart, next_wp_cart, lon)
            if t < 1:
                t_reflect_candidates.append((t, 0))
            # see if there are any reflexions with t < 1, i.e., between last_wp and next_wp
            if not t_reflect_candidates:
                break
            t, course_reflector = min(t_reflect_candidates)
            last_t = (1-t)*last_t + t * next_t
            last_wp_cart = Slerp(last_wp, next_wp)(t)
            last_wp = to_latlon(last_wp_cart)
            yield (last_t, *last_wp)
            course = get_course(last_wp, next_wp)
            distance = angle(last_wp_cart, next_wp_cart)
            next_wp_cart = propagate_course_distance(last_wp, course_reflector - course, distance)
            next_wp = to_latlon(next_wp_cart)
        assert lat_range[0]-eps < next_wp[0] < lat_range[1] + eps
        assert lon_range[0]-eps < next_wp[1] < lon_range[1] + eps
        yield (next_t, *next_wp)
        last_t = next_t
        last_wp = next_wp
        last_wp_cart = next_wp_cart
        if next_t >= duration:
            return

def random_waypoint_paths_to_file(path, num_nodes,
                                  lat_range, lon_range, vmax, tmax, duration,
                                  seed=None,
                                  keep=False):
    """Create num_nodes random waypoint paths.

    vmax is given in m/s and is converted to rad/s before feeding it into
         random_waypoint_path()

    lat_range and lon_range are given in rad

    """
    assert path.endswith('.sph.dat')
    if seed is not None:
        np.random.seed(seed)
    res = []
    lines = []
    for i in range(num_nodes):
        waypoints = random_waypoint_path(np.array(lat_range),
                                         np.array(lon_range),
                                         meters_per_second2rad_per_second(vmax), tmax, duration)
        if keep:
            waypoints = list(waypoints)
        res.append(waypoints)
        for t, lat, lon in waypoints:
            lines.append((t, i, lat, lon))
    lines.sort()
    with open(path, 'w') as file:
        file.write(f'latlon version 1.00 n_nodes {num_nodes:d} r {R_EARTH_MEAN:f} seed {seed}\n')
        for line in lines:
            # file.write('{:.5f} {:d} {:.12f} {:.12f}\n'.format(*line))
            file.write('{:.4f} {:d} {:.10f} {:.10f}\n'.format(*line))
    if keep:
        return res
    return None

class SlerpWaypointsInterpolator:
    def __init__(self, waypoints, R=None):
        self.waypoints = waypoints
        self.it = iter(waypoints)
        self.t1, *self.x1 = next(self.it)
        self.t2, *self.x2 = next(self.it)
        self.sl = None
        self.R = R
        self.delta_t = None
    def get_position(self, t, check=False):
        if t < self.t1:
            raise ValueError('cannot go back in time')
        while t > self.t2:
            self.t1, self.x1 = self.t2, self.x2
            self.t2, *self.x2 = next(self.it)
            self.sl = None
        if self.sl is None:
            self.sl = Slerp(to_cart_S2(self.x1), to_cart_S2(self.x2))
            self.delta_t = self.t2 - self.t1
        tau = (t - self.t1) / self.delta_t
        if self.R is None:
            return self.sl(tau, check)
        else:
            return self.R * self.sl(tau, check)


def compressed_cdf(x, xmin=None, xmax=None, delta=0.001):
    """Create data points for a cdf plot of samples x.

    If len(x) is large, a naive cdf plot of x produces large vector graphics.
    This function produced plot data that looks approximately equal using only
    up to 2/delta data points.

    """
    full_x = np.sort(x)
    if xmin is None:
        xmin = full_x[0]
    if xmax is None:
        xmax = full_x[-1]
    Lx = xmax - xmin
    assert Lx > 0
    full_y = np.linspace(0, 1, len(full_x), endpoint=False)
    cx = [full_x[0]]
    cy = [full_y[0]]
    for x, y in zip(full_x, full_y):
        if x < xmin:
            continue
        if (x-cx[-1])/Lx > delta or (y-cy[-1]) > delta:
            cx.append(x)
            cy.append(y)
        if x >= xmax:
            break
    if cy[-1] < full_y[-1] and full_x[-1] <= xmax:
        cx.append(full_x[-1])
        cy.append(full_y[-1])
    assert(sum((z > xmax for z in cx), 0) <= 1)
    return cx, cy

def measure_inhomogenity(N, path_dat=None):
    """Measure the spacial node distribution of a box-reflected random walk.
    """
    t = 20
    lat_range = (0.2*np.pi, 0.4 * np.pi)
    lon_range = (0, 0.1 * np.pi)
    z = [SlerpWaypointsInterpolator(random_waypoint_path(lat_range, lon_range, 1, 1, t+1)).get_position(t)[2]
         for _ in range(N)]
    x, y = compressed_cdf(z)
    if path_dat is not None:
        with open(path_dat, 'w') as file:
            for a, b in zip(x,y):
                print(f'{a:.3f} {b:.3f}', file=file)
    return z
