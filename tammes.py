#!/usr/bin/env python3

import numbers

import numpy as np
from numpy import pi, sin, cos, arcsin, arccos, arctan, arctan2, dot, cross, exp, log, log2, inner
from numpy.linalg import norm
from math import sqrt
import random

from typing import Iterable, Tuple, List

def vec3(x: float, y: float, z: float) -> np.ndarray:
    return np.array((x, y, z))

def normalized(vec: np.ndarray) -> np.ndarray:
    return vec / norm(vec)

def squared_norm(vec: np.ndarray) -> np.ndarray:
    return inner(vec, vec)

def calc_coverage(n: int, radius: float) -> float:
    circle_area = 2 * pi * (1 - cos(radius))
    total_area = circle_area * n
    sphere_area = 4 * pi
    coverage = total_area / sphere_area
    return coverage

def gcs2eci(lat: float, lon: float) -> np.ndarray:
    cos_lat = cos(lat)
    x = cos_lat * cos(lon)
    y = cos_lat * sin(lon)
    z = sin(lat)
    return vec3(x, y, z)

def eci2gcs(pos: np.ndarray) -> Tuple[float, float]:
    x, y, z = pos / norm(pos)
    lat = arcsin(z)
    lon = arctan2(y, x)
    return (lat, lon)

def r_circle_to_sphere(r_circle: float) -> float:
    """
    Calculates the radius of a sphere on the unit sphere which when intersected produces a circle on the sphere with
    radius r_circle.

    r_circle : radius of the circle on the unit sphere
    """

    return sin(r_circle / 2) * 2

def intersect_circles_on_sphere(r: float, lat: float, lon_between: float) -> bool:
    """
    Check if two circles with the same latitude and same size on the unit sphere intersect in more than one point by
    intersecting a sphere with a slice of the unit sphere.
    
    r : radius of the circles
    lat : latitude of the circles
    lon_between : longitude between the circles
    """

    center_sphere = gcs2eci(lat, lon_between * 0.5)
    radius_sphere = r_circle_to_sphere(r)
    n = vec3(0, 1, 0)
    d = dot(n, center_sphere)

    return d < radius_sphere

def pack_naive(r: float) -> Tuple[int, List[Tuple[int, float, float]]]:
    """
    Naive approach to the tammes problem.
    Coverage for r -> 0: pi / 4

    r : radius of the circles on the unit sphere

    Result : total number of packed circles, (number of circles, latitude, longitude offset (always 0)) for each row

    This is Max Schlecht's version
    """

    d = r * 2
    # start with a circle at each pole
    n = 2
    rows = [(1, -pi/2, 0), (1, pi/2, 0)]

    n_rows = int((pi - d) / d)
    for i in range(n_rows):
        lat = (i / (n_rows - 1) * 2 - 1) * (pi / 2 - d)
        circumference = 2 * pi * cos(lat)

        n_new = int(circumference / d)
        if n_new < 1:
            break

        if n_new > 1 and intersect_circles_on_sphere(r, lat, 2 * pi / n_new):
            n_new -= 1

        n += n_new
        rows.append((n_new, lat, 0))

    # order rows properly
    rows.append(rows.pop(1))

    return n, rows

def num_points_in_layer(L: float, theta: float) -> numbers.Real:
    """Return maximum number of points that can be placed at S2-distance L
    on a circle of latitude theta."""
    if np.pi/2 - np.abs(theta) < L/2:
        return 1
    return np.pi / np.arcsin(np.sin(L/2) / np.cos(theta))

def pack_naive_holger(r: float) -> Tuple[int, List[Tuple[int, float, float]]]:
    """
    Naive approach to the tammes problem.
    Coverage for r -> 0: pi / 4

    r : radius of the circles on the unit sphere

    Result : total number of packed circles, (number of circles, latitude, longitude offset (always 0)) for each row
    """
    L = 2 * r
    num_layers = int(1 + np.floor(np.pi / L))
    i = np.arange(num_layers)
    theta = (i + (1-num_layers)/2) * L
    n = [int(num_points_in_layer(L, theta_i))
         for theta_i in theta]
    return sum(n), [(n_i, theta_i, 0.0) for n_i, theta_i in zip(n, theta)]

def pack_improved(r: float, ks: Iterable[int]=range(1, 30, 2)) -> Tuple[int, List[Tuple[int, float, float]]]:
    """
    Improved approach to the tammes problem.
    Offsets rows of circles to maximize coverage. Each row of circles consists of k * 2 ** x circles.
    Computes packing for all ks and returns the best one.
    Coverage for r -> 0: ~0.855

    r : radius of the circles on the unit sphere
    ks : should be a list of small, odd numbers

    Result: number of packed circles, (number of circles, latitude, longitude offset) for each row
    """

    best_n = 0
    best_rows = []
    for k in ks:
        n, rows = pack_improved_internal(r, k)
        if n > best_n:
            best_n = n
            best_rows = rows

    return best_n, best_rows

def pack_improved_internal(r: float, k: int) -> Tuple[int, List[Tuple[int, float, float]]]:
    """
    Improved approach to the tammes problem.
    Offsets rows of circles to maximize coverage. Each row of circles consists of k * 2 ** x circles.

    r : radius of the circles on the unit sphere
    k : should be a small, odd number

    Result: number of packed circles, (number of circles, latitude, longitude offset) for each row
    """

    def find_next(lat_a: float, lon_a: float, lat_b: float, lon_b: float, r: float) -> Tuple[bool, Tuple[float, float]]:
        """
        Finds possible latitude and longitude offset of the next row by intersecting 3 spheres.
        algorithm from: https://stackoverflow.com/a/18654302

        Result : (success, (latitude, longitude offset))
        """

        center_a = gcs2eci(lat_a, lon_a)
        center_b = gcs2eci(lat_b, lon_b)
        center_c = vec3(0, 0, 0)
        r_ab = r_circle_to_sphere(r) * 2
        r_c = 1

        dir_x = center_b - center_a
        e_x = normalized(dir_x)
        
        dir_y = center_c - center_a
        i = dot(e_x, dir_y)
        e_y = normalized(dir_y - i * e_x)
        
        e_z = cross(e_x, e_y)

        d = norm(dir_x)
        j = dot(e_y, dir_y)

        x = (r_ab * r_ab - r_ab * r_ab + d * d) / (2 * d)
        y = (r_ab * r_ab - r_c * r_c - 2 * i * x + i * i + j * j) / (2 * j)

        z_squared = r_ab * r_ab - x * x - y * y
        if z_squared < 0: # no intersection
            return (False, (0, 0))
        z = sqrt(z_squared)

        p1 = center_a + x * e_x + y * e_y + z * e_z
        p2 = center_a + x * e_x + y * e_y - z * e_z

        # select the intersection point with the largest latitude
        p = p1 if p1[2] > p2[2] else p2
        
        # if the calculated latitude is < lat_a or lat_b, ignore it
        new_lat = arcsin(p[2])
        if new_lat < lat_a or new_lat < lat_b:
            return (False, (0, 0))

        return (True, eci2gcs(p))

    def compute_hemisphere(r: float, k: int, exponent: int, rows: List[Tuple[int, float, float]]=[]) -> Tuple[int, List[Tuple[int, float, float]]]:
        """
        Compute packing on one hemisphere.

        Result : total number of packed circles, (number of circles, latitude, longitude offset (always 0)) for each row
        """

        d = r * 2

        n = 0
        lat = 0
        lon_offset = 0
        while True:
            n_new = k * 2 ** exponent
            n += n_new
            rows.append((n_new, lat, lon_offset))

            found_a, next_a = find_next(lat, lon_offset, lat, lon_offset + 2 * pi / n_new, r)
            if len(rows) >= 2:
                found_b, next_b = find_next(lat, lon_offset, rows[-2][1], rows[-2][2] + 2 * pi / rows[-2][0], r)
            else:
                found_b = False

            if found_a and found_b:
                next_lat, next_lon_offset = next_a if next_a[0] > next_b[0] else next_b
            elif found_a:
                next_lat, next_lon_offset = next_a
            elif found_b:
                next_lat, next_lon_offset = next_b
            else:
                break
            next_lon_offset = next_lon_offset % (2 * pi / n_new)
            if next_lon_offset - (2 * pi / n_new) > -1e-7:
                next_lon_offset -= (2 * pi / n_new)
            while intersect_circles_on_sphere(r, next_lat, (2 * pi) / (k * 2 ** exponent)) and exponent > -2:
                exponent -= 1

            if exponent < 0 or pi / 2 - next_lat < r:
                break

            lat = next_lat
            lon_offset = next_lon_offset

        # try to fit some more rows naively
        for _ in range(int((pi / 2 - lat - r) / d)):
            lat += d
            n_new = int(2 * pi * cos(lat) / d) - 1

            while n_new > 0 and intersect_circles_on_sphere(r, lat, (2 * pi) / n_new):
                n_new -= 1
            
            if n_new == 0:
                break

            rows.append((n_new, lat, 0))
            n += n_new

        # check if we can fit one more circle at the pole
        if lat < (pi / 2 - d):
            rows.append((1, pi / 2, 0))
            n += 1

        return n, rows

    d = r * 2
    circumference = 2 * pi
    exponent = int(log2(circumference / d / k))

    n_a, rows_a = compute_hemisphere(r, k, exponent)
    n_b, rows_b = compute_hemisphere(r, k, exponent, rows=[(rows_a[1][0], -rows_a[1][1], -rows_a[1][2])])

    # mirror the second hemisphere and combine them
    rows = list(map(lambda row: (row[0], -row[1], row[2]), reversed(rows_b[2:]))) + rows_a

    return n_a + n_b - rows_b[1][0], rows

def row2gcs(row: Tuple[int, float, float], index: int) -> Tuple[float, float]:
    return (row[1], row[2] + 2 * pi * index / row[0])

def row2gcss(row: Tuple[int, float, float]) -> List[Tuple[float, float]]:
    return [(row[1], row[2] + 2 * pi * i / row[0]) for i in range(row[0])]

def find_nearest_point(lat: float, lon: float, points: List[Tuple[float, float]]) -> int:
    eci = gcs2eci(lat, lon)

    nearest = 0
    min_distance = squared_norm(eci - gcs2eci(*points[0]))
    for i, point in enumerate(points[1:]):
        distance = squared_norm(eci - gcs2eci(*point))
        if distance < min_distance:
            nearest = i + 1
            min_distance = distance

    return nearest

def find_nearest_neighbor_safe(lat: float, lon: float, rows: List[Tuple[int, float, float]]) -> Tuple[float, float]:
    """
    Safely finds the nearest neighbor of a point from a set of rows on a sphere by calculating the distance to each of them.
    
    lat : latitude of the point
    lon : longitude of the point
    rows : (number of circles, latitude, longitude offset) for each row of points on a sphere, sorted by latitude

    Result : (row index, column index) of the nearest neighbor
    """

    column_indices = [find_nearest_point(lat, lon, row2gcss(row)) for row in rows]
    row_index = find_nearest_point(lat, lon, [row2gcs(row, index) for row, index in zip(rows, column_indices)])

    return (row_index, column_indices[row_index])

def find_nearest_neighbor_fast(lat: float, lon: float, rows: List[Tuple[int, float, float]]) -> Tuple[float, float]:
    """
    Finds the nearest neighbor of a point from a set of rows on a sphere in approx. O(1).
    
    lat : latitude of the point
    lon : longitude of the point
    rows : (number of circles, latitude, longitude offset) for each row of points on a sphere, sorted by latitude

    Result : (row index, column index) of the nearest neighbor
    """

    n_rows = len(rows)

    # special case for poles
    if abs(lat + (pi / 2)) < 0.001:
        return (0, find_nearest_point(lat, lon, row2gcss(rows[0])))
    if abs(lat - (pi / 2)) < 0.001:
        return (n_rows - 1, find_nearest_point(lat, lon, row2gcss(rows[n_rows - 1])))

    row_index_a = min(max(1, round(n_rows * (lat / pi + 0.5))), n_rows - 2)

    direction = int(rows[row_index_a][1] < lat) * 2 - 1
    while row_index_a > 0 and row_index_a < n_rows - 1 and rows[row_index_a][1] * direction < lat * direction:
        row_index_a += direction

    row_index_b = min(max(0, row_index_a + direction), n_rows - 1)
    row_index_c = min(max(0, row_index_a - direction), n_rows - 1)
    row_index_d = min(max(0, row_index_a - direction * 2), n_rows - 1)

    row_indices = list({row_index_a, row_index_b, row_index_c, row_index_d})
    lon_indices = (round(n_row * (lon - lon_offset) / (2 * pi)) % n_row for n_row, lat, lon_offset in (rows[index] for index in row_indices))
    results = list(zip(row_indices, lon_indices))

    return results[find_nearest_point(lat, lon, [row2gcs(rows[row_index], lon_index) for row_index, lon_index in results])]

if __name__ == "__main__":
    from argparse import ArgumentParser
    from mayavi.mlab import points3d, show

    parser = ArgumentParser(description="Implementation of two methods for solving the tammes problem.")
    parser.add_argument("-r", "--radius", default=None, type=float)
    parser.add_argument("-s", "--solver", choices=("naive", "improved"), default="improved")
    parser.add_argument("-c", "--colorful", action="store_true")
    parser.add_argument("-k", default=None, type=int)
    parser.add_argument("-n", "--neighbor", default=None, type=float, nargs=2)
    args = parser.parse_args()

    if args.radius:
        radius = args.radius
    else:
        radius = random.uniform(0.05, 0.1)

    if args.colorful:
        gradient = []
        gradient_n = 16
        sat = 0.6
        for i in range(gradient_n):
            x = i / gradient_n * 2 * pi
            gradient.append(tuple(vec3(sin(x), sin(x + 2 / 3 * pi), sin(x + 4 / 3 * pi)) * sat * 0.5 + 0.5))
    else:
        gradient = [(0.4, 0.4, 0.4)]
        gradient_n = 1

    if args.solver == "naive":
        n, rows = pack_naive(radius)
        if args.k:
            print("Warning: -k argument is only qualified to be used with the \"improved\" solver")

    elif args.solver == "improved":
        if args.k:
            n, rows = pack_improved_internal(radius, args.k)
        else:
            n, rows = pack_improved(radius)

    print(f"radius: {radius}, solver: {args.solver}")
    print(f"number of circles: {n}")
    print(f"coverage: {calc_coverage(n, radius):.4f}")

    if args.neighbor:
        if args.solver == "improved":
            neighbor = find_nearest_neighbor_safe(args.neighbor[0], args.neighbor[1], rows)
            print(f"nearest neighbor: {neighbor}")
        else:
            print("Warning: -n argument is only qualified to be used with the \"improved\" solver")

    points3d(0, 0, 0, scale_factor=2, resolution=256, color=(0.8, 0.8, 0.8))
    for i, row in enumerate(rows):
        points3d(*zip(*[gcs2eci(*gcs) for gcs in row2gcss(row)]), scale_factor=r_circle_to_sphere(radius)*2, resolution=64, color=gradient[i % gradient_n])

    show()

def test_plots():
    from matplotlib import pyplot as plt

    samples = 50
    random.seed(1337)
    rs = sorted([1 / random.uniform(5, 1000) for _ in range(samples)] + [random.uniform(0.001, 0.2) for _ in range(samples * 4)])

    naive_ns = list(zip(*map(pack_naive, rs)))[0]
    naive_cs = list(map(calc_coverage, naive_ns, rs))
    plt.plot(naive_ns, naive_cs, color="royalblue")

    better_ns = list(zip(*map(pack_improved, rs)))[0]
    better_cs = list(map(calc_coverage, better_ns, rs))
    plt.plot(better_ns, better_cs, color="purple")

    plt.legend(("naive", "improved"))

    plt.hlines([pi/4], naive_ns[0], naive_ns[-1], colors="red")
    plt.annotate("pi/4", (1000, 0.793), color="red")
    plt.hlines([pi * sqrt(3) / 6], naive_ns[0], naive_ns[-1], colors="red")
    plt.annotate("pi * sqrt(3) / 6", (1000, 0.915), color="red")

    plt.xlabel("number of circles")
    plt.xscale("log")
    plt.xlim(5e1, 5e6)
    plt.ylabel("coverage")
    plt.ylim(0.65, 0.93)
    plt.savefig("results.pdf")

def test_neighbors():
    random.seed(1337)

    for _ in range(100):
        radius = random.uniform(0.025, 0.1)
        n, rows = pack_improved(radius)

        for lat, lon in ((random.uniform(-pi/2, pi/2), random.uniform(0, 2 * pi)) for _ in range(100)):
            neighbor = find_nearest_neighbor_safe(lat, lon, rows)
            neighbor_fast = find_nearest_neighbor_fast(lat, lon, rows)
            assert neighbor == neighbor_fast

def test_packing(r, packing, inaccuracy_margin=1e-8):
    """Exhaustive numerical test if points of packing are mutually >= 2*r apart."""
    min_dist_sqr = (2 * np.sin(r))**2 * (1-inaccuracy_margin)
    points = []
    for n, theta, Delta_phi in packing:
        for phi in (np.arange(n) * 2 * np.pi / n + Delta_phi):
            points.append(gcs2eci(theta, phi))
    for i, p1 in enumerate(points):
        for p2 in points[:i]:
            assert np.sum((p1 - p2)**2) >= min_dist_sqr
