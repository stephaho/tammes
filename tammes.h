#ifndef __TAMMES_HPP
#define __TAMMES_HPP

#include "vec3.h"

#include <iostream>
#include <stdint.h>
#include <vector>

namespace tammes {
struct gcs;
struct eci;
struct row;
struct packing;
struct neighbor;
struct extended_row;
class extended_packing;

double calc_coverage(const uint32_t n, const double radius);
double r_circle_to_sphere(const double r);
bool intersect_circles_on_sphere(const double r, const double lat,
                                 const double lon_between);

packing pack(const double r,
             const std::vector<uint32_t> &ks = {1, 3, 5, 7, 9, 11, 13, 15, 17,
                                                19, 21, 23, 25, 27, 29});
packing pack(const double r, const uint32_t k);

neighbor find_nearest_neighbor_safe(const gcs &target,
                                    const std::vector<row> &rows);
neighbor find_nearest_neighbor_fast(const gcs &target,
                                    const std::vector<row> &rows);
neighbor find_nearest_neighbor_faster(const gcs &target,
                                      const std::vector<row> &rows,
                                      const double r2);
neighbor find_nearest_neighbor_faster(const eci &target,
                                      const std::vector<extended_row> &rows,
                                      const double r2);
neighbor find_nearest_neighbor_faster(const gcs &target,
                                      const std::vector<extended_row> &rows,
                                      const double r2);
// neighbor find_nearest_neighbor_faster(const gcs &target_gcs,
//                                       const eci &target_eci,
//                                       const std::vector<extended_row> &rows,
//                                       const double r2);

constexpr double PI = 3.14159265358979323846;
constexpr double PI_2 = PI * 0.5;
constexpr double TAU = PI * 2.0;

struct neighbor {
  size_t row_index;
  size_t column_index;

  friend bool operator==(const neighbor &a, const neighbor &b);
  friend std::ostream &operator<<(std::ostream &stream,
                                  const neighbor &neighbor);
};

struct packing {
  uint32_t n;
  std::vector<row> rows;
  gcs neighbor_to_gcs(const neighbor &n) const;
  double get_min_dist() const;
};

/**
 * \brief spherical coordinates with implicitly assumed unit radius
 */
struct gcs {
  double lat, lon;

  eci to_eci() const;
  static double spherical_distance(const gcs &a, const gcs &b);
};

/**
 * \brief cartesian unit vector
 */
struct eci {
  vec3 co;

  gcs to_gcs() const;
  bool approx_equal(const eci &other) const;
};

struct row {
  uint32_t n;
  gcs start;

  gcs to_gcs(const uint32_t index) const;
  std::vector<gcs> to_gcs() const;
};

class xy {
public:
  xy(const double _x, const double _y);
  double x;
  double y;
};

class rot2d {
public:
  rot2d(const double phi);
  xy rotate(const xy &v) const;

private:
  double cos_phi;
  double sin_phi;
};

struct extended_row {
  extended_row(uint16_t _n, double _theta, double _phi_offset);
  uint16_t n; // # points in row
  uint8_t k;  // largest odd factor of n
  uint8_t j;  // n == k << j
  double cos_theta;
  double sin_theta;
  double phi_offset;
  rot2d phi_offset_matrix;
  // double delta_phi;
  std::vector<xy> *xy_cache;
  uint8_t cache_stride_log2;

  eci to_eci(const uint32_t index) const;
};

class extended_packing {
public:
  extended_packing(const packing &p);
  uint32_t n;
  std::vector<extended_row> rows;
  std::vector<std::vector<xy>> xy_caches;
};

} // namespace tammes

#endif // __TAMMES_HPP
