#define PICOBENCH_IMPLEMENT_WITH_MAIN
#include "picobench.h"

#include "tammes.h"
#include <random>

#define NUM_ITERATIONS 1024

void bench_nearest_neighbor_safe(picobench::state &state, double radius) {
  tammes::packing p = tammes::pack(radius);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution lat_distribution(-tammes::PI_2, tammes::PI_2);
  std::uniform_real_distribution lon_distribution(0.0, tammes::TAU);

  picobench::scope scope(state);
  for (int i = 0; i < state.iterations(); i++) {
    double lat = lat_distribution(gen);
    double lon = lon_distribution(gen);
    tammes::find_nearest_neighbor_safe(tammes::gcs{lat, lon}, p.rows);
  }
}

void bench_nearest_neighbor_fast(picobench::state &state, double radius) {
  tammes::packing p = tammes::pack(radius);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution lat_distribution(-tammes::PI_2, tammes::PI_2);
  std::uniform_real_distribution lon_distribution(0.0, tammes::TAU);

  picobench::scope scope(state);
  for (int i = 0; i < state.iterations(); i++) {
    double lat = lat_distribution(gen);
    double lon = lon_distribution(gen);
    tammes::find_nearest_neighbor_fast(tammes::gcs{lat, lon}, p.rows);
  }
}

void bench_nearest_neighbor_faster(picobench::state &state, double radius) {
  const double r2 = radius * radius;
  tammes::packing p = tammes::pack(radius);

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution lat_distribution(-tammes::PI_2, tammes::PI_2);
  std::uniform_real_distribution lon_distribution(0.0, tammes::TAU);

  picobench::scope scope(state);
  for (int i = 0; i < state.iterations(); i++) {
    double lat = lat_distribution(gen);
    double lon = lon_distribution(gen);
    tammes::find_nearest_neighbor_faster(tammes::gcs{lat, lon}, p.rows, r2);
  }
}

void bench_nearest_neighbor_faster_ext(picobench::state &state, double radius) {
  const double r2 = radius * radius;
  tammes::packing p = tammes::pack(radius);
  tammes::extended_packing ep{p};

  std::random_device rd;
  std::mt19937 gen(rd());
  std::uniform_real_distribution lat_distribution(-tammes::PI_2, tammes::PI_2);
  std::uniform_real_distribution lon_distribution(0.0, tammes::TAU);

  picobench::scope scope(state);
  for (int i = 0; i < state.iterations(); i++) {
    double lat = lat_distribution(gen);
    double lon = lon_distribution(gen);
    tammes::find_nearest_neighbor_faster(tammes::gcs{lat, lon}, ep.rows, r2);
  }
}

void neighbors_safe_0_100(picobench::state &state) {
  bench_nearest_neighbor_safe(state, 0.100);
}
void neighbors_safe_0_050(picobench::state &state) {
  bench_nearest_neighbor_safe(state, 0.050);
}
void neighbors_safe_0_025(picobench::state &state) {
  bench_nearest_neighbor_safe(state, 0.025);
}
void neighbors_safe_0_010(picobench::state &state) {
  bench_nearest_neighbor_safe(state, 0.010);
}
void neighbors_safe_0_005(picobench::state &state) {
  bench_nearest_neighbor_safe(state, 0.005);
}

void neighbors_fast_0_100(picobench::state &state) {
  bench_nearest_neighbor_fast(state, 0.100);
}
void neighbors_fast_0_050(picobench::state &state) {
  bench_nearest_neighbor_fast(state, 0.050);
}
void neighbors_fast_0_025(picobench::state &state) {
  bench_nearest_neighbor_fast(state, 0.025);
}
void neighbors_fast_0_010(picobench::state &state) {
  bench_nearest_neighbor_fast(state, 0.010);
}
void neighbors_fast_0_005(picobench::state &state) {
  bench_nearest_neighbor_fast(state, 0.005);
}

void neighbors_faster_0_100(picobench::state &state) {
  bench_nearest_neighbor_faster(state, 0.100);
}
void neighbors_faster_0_050(picobench::state &state) {
  bench_nearest_neighbor_faster(state, 0.050);
}
void neighbors_faster_0_025(picobench::state &state) {
  bench_nearest_neighbor_faster(state, 0.025);
}
void neighbors_faster_0_010(picobench::state &state) {
  bench_nearest_neighbor_faster(state, 0.010);
}
void neighbors_faster_0_005(picobench::state &state) {
  bench_nearest_neighbor_faster(state, 0.005);
}

void neighbors_faster_ext_0_100(picobench::state &state) {
  bench_nearest_neighbor_faster_ext(state, 0.100);
}
void neighbors_faster_ext_0_050(picobench::state &state) {
  bench_nearest_neighbor_faster_ext(state, 0.050);
}
void neighbors_faster_ext_0_025(picobench::state &state) {
  bench_nearest_neighbor_faster_ext(state, 0.025);
}
void neighbors_faster_ext_0_010(picobench::state &state) {
  bench_nearest_neighbor_faster_ext(state, 0.010);
}
void neighbors_faster_ext_0_005(picobench::state &state) {
  bench_nearest_neighbor_faster_ext(state, 0.005);
}

// PICOBENCH(neighbors_safe_0_100).iterations({NUM_ITERATIONS});
// PICOBENCH(neighbors_safe_0_050).iterations({NUM_ITERATIONS});
// PICOBENCH(neighbors_safe_0_025).iterations({NUM_ITERATIONS});
// PICOBENCH(neighbors_safe_0_010).iterations({NUM_ITERATIONS});
// PICOBENCH(neighbors_safe_0_005).iterations({NUM_ITERATIONS});

PICOBENCH(neighbors_fast_0_100).iterations({NUM_ITERATIONS});
PICOBENCH(neighbors_fast_0_050).iterations({NUM_ITERATIONS});
PICOBENCH(neighbors_fast_0_025).iterations({NUM_ITERATIONS});
PICOBENCH(neighbors_fast_0_010).iterations({NUM_ITERATIONS});
PICOBENCH(neighbors_fast_0_005).iterations({NUM_ITERATIONS});

PICOBENCH(neighbors_faster_0_100).iterations({NUM_ITERATIONS * 64});
PICOBENCH(neighbors_faster_0_050).iterations({NUM_ITERATIONS * 64});
PICOBENCH(neighbors_faster_0_025).iterations({NUM_ITERATIONS * 64});
PICOBENCH(neighbors_faster_0_010).iterations({NUM_ITERATIONS * 64});
PICOBENCH(neighbors_faster_0_005).iterations({NUM_ITERATIONS * 64});

PICOBENCH(neighbors_faster_ext_0_100).iterations({NUM_ITERATIONS * 64});
PICOBENCH(neighbors_faster_ext_0_050).iterations({NUM_ITERATIONS * 64});
PICOBENCH(neighbors_faster_ext_0_025).iterations({NUM_ITERATIONS * 64});
PICOBENCH(neighbors_faster_ext_0_010).iterations({NUM_ITERATIONS * 64});
PICOBENCH(neighbors_faster_ext_0_005).iterations({NUM_ITERATIONS * 64});
