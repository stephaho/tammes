#include "vec3.h"

#include <cmath>

constexpr double TWO_PI = 2.0 * M_PI;

namespace tammes {
double vec3::length() const { return sqrt(this->length_sq()); }

double vec3::length_sq() const { return vec3::dot(*this, *this); }

vec3 vec3::normalized() const { return *this / this->length(); }

double vec3::dot(const vec3 &a, const vec3 &b) {
  return a.x * b.x + a.y * b.y + a.z * b.z;
}

vec3 vec3::cross(const vec3 &a, const vec3 &b) {
  return vec3{a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z,
              a.x * b.y - a.y * b.x};
}

double vec3::distance(const vec3 &a, const vec3 &b) { return (a - b).length(); }

double vec3::distance_sq(const vec3 &a, const vec3 &b) {
  return (a - b).length_sq();
}

vec3 vec3::operator+(const vec3 &rhs) const {
  return vec3{this->x + rhs.x, this->y + rhs.y, this->z + rhs.z};
}

vec3 vec3::operator-(const vec3 &rhs) const {
  return vec3{this->x - rhs.x, this->y - rhs.y, this->z - rhs.z};
}

vec3 vec3::operator*(const double &rhs) const {
  return vec3{this->x * rhs, this->y * rhs, this->z * rhs};
}

vec3 operator*(const double &lhs, const vec3 &rhs) { return rhs * lhs; }

vec3 vec3::operator/(const double &rhs) const {
  return vec3{this->x / rhs, this->y / rhs, this->z / rhs};
}

std::ostream &operator<<(std::ostream &os, const vec3 &v) {
  return os << "[" << v.x << ", " << v.y << ", " << v.z << "]";
}

vec3 mat3x3::lmult_vec3(const vec3 &v) const {
  return vec3(a00 * v.x + a01 * v.y + a02 * v.z,
              a10 * v.x + a11 * v.y + a12 * v.z,
              a20 * v.x + a21 * v.y + a22 * v.z);
}

vec3 mat3x3::lmult_T_vec3(const vec3 &v) const {
  return vec3(a00 * v.x + a10 * v.y + a20 * v.z,
              a01 * v.x + a11 * v.y + a21 * v.z,
              a02 * v.x + a12 * v.y + a22 * v.z);
}

void mat3x3::set_random_rotation(double r0, double r1, double r2) {
  /* This is basically a copy of John Arvo's 1991 rand_rotation.

     http://audilab.bmed.mcgill.ca/~funnell/graphics/GraphicsGems/private/3/gemsiii/gemsiii/rand_rotation.c
   */
  const double theta = r0 * TWO_PI; /* Rotation about the pole (Z).      */
  const double phi = r1 * TWO_PI;   /* For direction of pole deflection. */
  const double z = r1 * 2.0;        /* For magnitude of pole deflection. */

  /* Compute a vector V used for distributing points over the sphere  */
  /* via the reflection I - V Transpose(V).  This formulation of V    */
  /* will guarantee that if x[1] and x[2] are uniformly distributed,  */
  /* the reflected points will be uniform on the sphere.  Note that V */
  /* has length sqrt(2) to eliminate the 2 in the Householder matrix. */

  const double r = sqrt(z);
  const double Vx = sin(phi) * r;
  const double Vy = cos(phi) * r;
  const double Vz = sqrt(2.0 - z);

  /* Compute the row vector S = Transpose(V) * R, where R is a simple */
  /* rotation by theta about the z-axis.  No need to compute Sz since */
  /* it's just Vz.                                                    */

  const double st = sin(theta);
  const double ct = cos(theta);
  const double Sx = Vx * ct - Vy * st;
  const double Sy = Vx * st + Vy * ct;

  /* Construct the rotation matrix  ( V Transpose(V) - I ) R, which   */
  /* is equivalent to V S - R.                                        */

  a00 = Vx * Sx - ct;
  a01 = Vx * Sy - st;
  a02 = Vx * Vz;

  a10 = Vy * Sx + st;
  a11 = Vy * Sy - ct;
  a12 = Vy * Vz;

  a20 = Vz * Sx;
  a21 = Vz * Sy;
  a22 = 1.0 - z; /* This equals Vz * Vz - 1.0 */
}
} // namespace tammes
