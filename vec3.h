#ifndef __VEC3_HPP
#define __VEC3_HPP

#include <ostream>

namespace tammes {
struct vec3 {
  double x, y, z;

  vec3(double _x, double _y, double _z) : x(_x), y(_y), z(_z){};

  double length() const;
  double length_sq() const;
  vec3 normalized() const;
  static double dot(const vec3 &a, const vec3 &b);
  static vec3 cross(const vec3 &a, const vec3 &b);
  static double distance(const vec3 &a, const vec3 &b);
  static double distance_sq(const vec3 &a, const vec3 &b);

  vec3 operator+(const vec3 &rhs) const;
  vec3 operator-(const vec3 &rhs) const;
  vec3 operator*(const double &rhs) const;
  friend vec3 operator*(const double &lhs, const vec3 &rhs);
  vec3 operator/(const double &rhs) const;
};

std::ostream &operator<<(std::ostream &os, const vec3 &v);

struct mat3x3 {
  double a00;
  double a01;
  double a02;
  double a10;
  double a11;
  double a12;
  double a20;
  double a21;
  double a22;
  /**
   * \brief multiply matrix with vector; return M * v
   */
  vec3 lmult_vec3(const vec3 &v) const;
  /**
   * \brief multiply transposed matrix with vector; return transposed(M) * v
   */
  vec3 lmult_T_vec3(const vec3 &v) const;
  /**
   * \brief replace matrix with random matrix
   *
   * the generated matrices are parameteriezed via uniform random variable
   * values r0, r1, r2
   */
  void set_random_rotation(double r0, double r1, double r2);
};

} // namespace tammes

#endif // __VEC3_HPP
